const express = require('express')
const bodyParsar = require('body-parser')
const PORT = 3000
const cors = require('cors')

const api = require('./routes/api')
const app = express()
app.use (cors())
app.use(bodyParsar.json())

app.use('/api',api)
app.get('/',function(req,resp){
    resp.send('Hello from server')
})
app.listen(PORT,function(){
    console.log('Server runnig on localhost : '+ PORT)
})