const express = require('express')
const jwt = require('jsonwebtoken')
const router = express.Router()
var mysql = require('mysql');
var md5 = require('md5');
// var con = mysql.createConnection({
  // host: "localhost",
  // user: "root",
  // password: "",
  // database: "test"
// });

// con.connect(function(err) {
  // if (err) throw err;
  // console.log("Connected!");
// });

function verifyToken(req, res, next){
    if(!req.headers.authorization){
        res.status(401).send('Unathorized request')
    }
    let token = req.headers.authorization.split(' ')[1]
    if(token === 'null'){
        res.status(401).send('Unathorized request')
    }
    let payload = jwt.verify(token,'ravi')
    if(!payload){
        res.status(401).send('Unathorized request')
    }
    req.userId = payload.subject
    next()
}

router.get('/',(req, resp)=>{
    resp.send('Respons from API route')
})


router.get('/event',(req,resp)=>{
    let events = [
        {
            "id":1,
            "name":"test",
            "discription":"test discription"
        },
        {
            "id":2,
            "name":"test",
            "discription":"test discription"
        },
        {
            "id":3,
            "name":"test",
            "discription":"test discription"
        },
        {
            "id":4,
            "name":"test",
            "discription":"test discription"
        },
        {
            "id":5,
            "name":"test",
            "discription":"test discription"
        },
        {
            "id":6,
            "name":"test",
            "discription":"test discription"
        }
    ]
    //resp.json(events)
    //let events = json(events)
    resp.status(200).send({events})
})
router.get('/special',verifyToken,(req,resp)=>{
    let events = [
        {
            "id":1,
            "name":"test",
            "discription":"test discription"
        },
        {
            "id":2,
            "name":"test",
            "discription":"test discription"
        },
        {
            "id":3,
            "name":"test",
            "discription":"test discription"
        },
        {
            "id":4,
            "name":"test",
            "discription":"test discription"
        },
        {
            "id":5,
            "name":"test",
            "discription":"test discription"
        },
        {
            "id":6,
            "name":"test",
            "discription":"test discription"
        }
    ]
    resp.json(events)
   // resp.status(200).send({events})
})

// router.post('/login',(req,resp)=>{
    // con.connect(function(err) {
       // let userData = req.body;
       // if(userData.email !==' ' && userData.password !==' ' ){
        // //Select all customers and return the result object:
        // var sql = `SELECT * FROM users WHERE email = '${userData.email}' AND password = '${md5(userData.password)}'`;
        // //resp.json(sql)
            // con.query(sql, function (err, result) {
                // if (err) {
                    // console.log(err)
                // }else{
                    // //  resp.json(result.length)
                    // if(result.length === 0){
                        // resp.status(401).send({'msg':'Invalid Email or Password'})
                    // }else{
                        // let payload = {subject : result.id}
                        // let token = jwt.sign(payload,'ravi')
                        // resp.status(200).send({token})
                    // }
                // }
            // });
        // }
    // });
// })

// router.post('/regiser',(req,resp)=>{
    // con.connect(function(err) {
        // var userData = req.body;
        // //Select all customers and return the result object:
        // var sql1 = `SELECT * FROM users WHERE email = '${userData.email}'`;
        // var sql = `INSERT INTO users (name, email,password) VALUES ('${userData.name}','${userData.email}', '${md5(userData.password)}')`;
       // // resp.json(sql)
        // con.query(sql1, function (err, result) {
            // if (err) {
                // console.log(err)
            // }else{
                // if(result.length === 0){
                    // con.query(sql, function (err1, result1) { 
                        // if (err1) {
                            // console.log(err1)
                        // }else{
                            // let payload = {subject : result1.id}
                            // let token = jwt.sign(payload,'ravi')
                           // resp.status(200).send({token})
                           
                        // }  
                    // })
                // }else{
                    // resp.status(401).send({'msg':'email already exits'})
                // }
                
               
            // }
        // });
    // });
// })

router.get('/event/:id',(req,resp)=>{
    
   // console.log(id);
    let events = [
        {
            "id":1,
            "name":"test",
            "discription":"test discription"
        },
        {
            "id":2,
            "name":"test",
            "discription":"test discription"
        },
        {
            "id":3,
            "name":"test",
            "discription":"test discription"
        },
        {
            "id":4,
            "name":"test",
            "discription":"test discription"
        },
        {
            "id":5,
            "name":"test",
            "discription":"test discription"
        },
        {
            "id":6,
            "name":"test",
            "discription":"test discription"
        }
    ]

    function search(nameKey, myArray){
        for (var i=0; i < myArray.length; i++) {
            if (myArray[i].id == nameKey) {
                return myArray[i];
            }
        }
    }
    
  
    let id = req.params.id;
    
    var resultObject = search(id, events);
    
    //console.log(resultObject);
    resp.json(resultObject);
   
})


module.exports = router