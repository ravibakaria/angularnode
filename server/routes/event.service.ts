import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 

@Injectable({
  providedIn: 'root'
})
export class EventService {
  private _eventUrl = "http://localhost:3000/api/event";
  private _specialUrl = "http://localhost:3000/api/special";
  private _eventByid = "http://localhost:3000/api/event/id";
  constructor(private http: HttpClient) { }


  getEvent(){
    return this.http.get<any>(this._eventUrl)
  }
  getSpecial(){
    return this.http.get<any>(this._specialUrl)
  }
  getEventByid(id){
    return this.http.get<any>(this._eventUrl+'/'+id)
  }
}
