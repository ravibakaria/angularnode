import { Injectable } from '@angular/core';
import { CanActivate, Router} from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate  { 
  constructor(private _auth: AuthService,
    private _router : Router) { }

  canActivate(): boolean{
    if(this._auth.loggedin()){
      this._router.navigate(['/events']);
      return false;
    }
    return true;
    
  }
}
