import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { EventsComponent } from './events/events.component';
import { SpecialEventsComponent } from './special-events/special-events.component';
import { AuthService } from './auth.service';
import { EventService } from './event.service';
import { AuthGuard } from './auth.guard';
import { TokenInterceptorService } from './token-interceptor.service';
import { EventsDetailComponent } from './events-detail/events-detail.component';
import { TestComponent } from './test/test.component';
import{ jqxGridComponent  } from 'jqwidgets-ng/jqxgrid';
import { jqxBarGaugeModule }    from 'jqwidgets-ng/jqxbargauge'; 
@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    jqxGridComponent,
    LoginComponent,
    EventsComponent,
    SpecialEventsComponent,
    EventsDetailComponent,
    TestComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    jqxBarGaugeModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [AuthService,AuthGuard, EventService,
  {
    provide : HTTP_INTERCEPTORS,
    useClass : TokenInterceptorService,
    multi : true
  }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
