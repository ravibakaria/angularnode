import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _registerUrl = "http://localhost:3000/api/regiser";
  private _loginUrl = "http://localhost:3000/api/login";

  constructor(private http: HttpClient,private _router:Router) { }

  register(user){
    let config = {
      headers: {
        "Content-Type": "application/json",
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': '*',
        'Access-Control-Allow-Methods': '*'
        }
      }
    //return console.log(user);
    return this.http.post<any>(this._registerUrl,user)
  }
  loin(user) {
    return this.http.post<any>(this._loginUrl,user)
  }

  loggedin(){
    return !!localStorage.getItem('token')
  }

  logoutuser(){
    localStorage.removeItem('token')
    this._router.navigate(['/login'])
  }

  getToken(){
    return localStorage.getItem('token')
  }

}
